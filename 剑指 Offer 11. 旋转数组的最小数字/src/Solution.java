import com.sun.crypto.provider.BlowfishKeyGenerator;

/**
 * Created by L.jp
 * Description:把一个数组最开始的若干个元素搬到数组的末尾，我们称之为数组的旋转。
 *
 * 给你一个可能存在 重复 元素值的数组 numbers ，它原来是一个升序排列的数组，
 * 并按上述情形进行了一次旋转。请返回旋转数组的最小元素。
 * 例如，数组 [3,4,5,1,2] 为 [1,2,3,4,5] 的一次旋转，该数组的最小值为 1。  
 * User: 86189
 * Date: 2022-11-15
 * Time: 18:07
 */
public class Solution {
    public int minArray(int[] numbers) {
        int left=0;
        int right=numbers.length-1;
        //关键点在于找旋转点，旋转点就是最小元素
        while (left < right) {
            int mid=left+(right-left)/2;
            if(numbers[mid]>numbers[right]){
                //旋转了数组，小的元素肯定在mid右边
                left=mid + 1;
            }else if(numbers[mid]<numbers[right]){
                //说明最小值就是在mid以及mid之前
                right=mid;
            }else{
                //如果nums[mid]==nums[right]，那么right往前走一个也没啥影响
                right--;
            }
        }
        //当left=right时，就找到了最小值的位置也就是旋转点
        return numbers[left];
    }
}
