import java.util.HashSet;
import java.util.Set;

/**
 * Created by L.jp
 * Description:找出数组中重复的数字。
 * 在一个长度为 n 的数组 nums 里的所有数字都在 0～n-1 的范围内。数组中某些数字是重复的，但不知道有几个数字重复了，也不知道每个数字重复了几次。请找出数组中任意一个重复的数字。

 * User: 86189
 * Date: 2022-11-10
 * Time: 23:56
 */
public class Solution {
    public static int findRepeatNumber(int[] nums) {
        //原地置换
       /* int temp;
        for(int i=0;i<nums.length;i++){
            //原地置换，按道理讲如果没有重复的话那就是i下标对应的元素就是i元素
            //现在有重复了，那么就要把元素归位，如果nums[i]与nums[nums[i]]不相等那么就是交换，相等就返回其中一个元素
            while(nums[i]!=i){
                if(nums[i]==nums[nums[i]]){
                    return nums[i];
                }
                temp=nums[i];
                nums[i]=nums[temp];
                nums[temp]=temp;
            }
        }*/
        //哈希表
        Set<Integer> set=new HashSet<>();
        for (int num : nums) {
            if (set.contains(num)) {
                return num;
            }
            set.add(num);
        }
        return -1;
    }
    
    public static void main(String[] args) {
        int[] nums={1,2,3,3,4};
        System.out.println(findRepeatNumber(nums));
    }

}
