import javax.security.auth.callback.CallbackHandler;

/**
 * Created by L.jp
 * Description:请实现一个函数，把字符串 s 中的每个空格替换成"%20"。
 * User: 86189
 * Date: 2022-11-12
 * Time: 17:53
 */
public class Solution {
    public static String replaceSpace(String s) {
        int len=s.length();
        char[] chars=s.toCharArray();
        StringBuilder concat=new StringBuilder();
        int i=0;
        while (i<len){
            if(chars[i]!=' '){
                concat.append(chars[i]);
            }else{
                concat.append("%20");
            }
            i++;
        }
        return concat.toString();
    }
    
    public static void main(String[] args) {
        String s="We are happy";
        System.out.println(replaceSpace(s));
    }
}
