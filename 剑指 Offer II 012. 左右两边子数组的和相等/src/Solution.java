/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2023-06-28
 * Time: 23:58
 */
public class Solution {
    public int pivotIndex(int[] nums) {
        int res = -1;
        int leftSum = 0;
        int rightSum = 0;
        for (int i = 0; i < nums.length; i++)
            rightSum += nums[i];
        for (int i = 0; i < nums.length; i++) {
            rightSum -= nums[i];
            if (leftSum == rightSum) {
                res = i;
                break;
            }
            leftSum += nums[i];
        }
        return res;
    }
}

