import java.util.Arrays;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-12-18
 * Time: 23:58
 */
public class Solution {
    public String minNumber (int[] nums) {
        int n = nums.length;
        Integer[] num = new Integer[n];//Arrays.sort的比较器需要传非基本类型
        for (int i = 0; i < n; i++) {
            num[i] = nums[i];
        }
        //java的Arrays.sort有优化，排序速度较快
        Arrays.sort( num , (o1 , o2) -> mergeNumber( o2 , o1 ) - mergeNumber( o1 , o2 ) );
        StringBuilder ans = new StringBuilder();
        for (int i = 0; i < n; i++) {
            ans.append( num[i] );
        }
        String s = new String( ans );
        return s;
    }
    
    //该方法返回o2拼在前面的结果
    public int mergeNumber (int o1 , int o2) {
        int index = 10;
        int n = o1;
        while ( o1 / 10 != 0 ) {
            o1 = o1 / 10;
            index *= 10;
        }
        return o2 * index + n;
    }
}
