/**
 * Created by L.jp
 * Description:一只青蛙一次可以跳上1级台阶，也可以跳上2级台阶。求该青蛙跳上一个 n 级的台阶总共有多少种跳法。
 *
 * 答案需要取模 1e9+7（1000000007），如计算初始结果为：1000000008，请返回 1。
 * User: 86189
 * Date: 2022-11-23
 * Time: 23:09
 */
public class Solution {
    //动态规划
    /* public static int getFibNumber(int n){
     *//* int[] fib=new int[101];
        fib[0]=1;
        fib[1]=1;
        for(int i=2;i<=n;i++){
            fib[i]=(fib[i-1]+fib[i-2])%1000000007;
        }
        return fib[n];*//*
    }*/
    //动态规划优化
    public static int numWays(int n) {
        if(n==0){
            return 1;
        }
        //优化
        int a=1;
        int b=1;
        int c;
        for(int i=2;i<=n;i++){
            c=a+b;
            a=b;
            b=c%1000000007;
        }
        return b;
    }
    
    public static void main (String[] args) {
        System.out.println( numWays(7) );
    }

}
