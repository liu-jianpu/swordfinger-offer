
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-11-18
 * Time: 23:53
 */
class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) { val = x; }
  }
public class Solution {
    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> ret=new LinkedList<>();
        if (root == null) {
            return ret;
        }
        Queue<TreeNode> nodeQueue=new LinkedList<>();
        nodeQueue.offer( root );
        while ( !nodeQueue.isEmpty() ){
            int size=nodeQueue.size();
            List<Integer> tmp = new LinkedList<>();
            while (size > 0) {
                TreeNode node = nodeQueue.poll();
                tmp.add( node.val );
                if ( node.left != null ) {
                    nodeQueue.add( node.left );
                }
                if ( node.right != null ) {
                    nodeQueue.add( node.right );
                }
                size--;
            }
            ret.add(tmp);
        }
        return ret;
    }
}
