import sun.reflect.generics.tree.Tree;

/**
 * Created by L.jp
 * Description:请完成一个函数，输入一个二叉树，该函数输出它的镜像。
 * User: 86189
 * Date: 2022-11-21
 * Time: 23:42
 */
class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) { val = x; }
}
public class Solution {
    public TreeNode mirrorTree(TreeNode root) {
        //递归，dfs
        if(root == null){
            return root;
        }
        //交换节点
        TreeNode tmp=root.left;
        root.left=root.right;
        root.right=tmp;
        //递归左右子树
        mirrorTree( root.left );
        mirrorTree( root.right );
        return root;
    }

}
