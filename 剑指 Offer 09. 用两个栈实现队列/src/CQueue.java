import java.util.Stack;

/**
 * Created by L.jp
 * Description:用两个栈实现一个队列。队列的声明如下，请实现它的两个函数 appendTail 和 deleteHead ，分别完成在队列尾部插入整数和在队列头部删除整数的功能。(若队列中没有元素，deleteHead 操作返回 -1 )
 *
 * User: 86189
 * Date: 2022-11-13
 * Time: 23:29
 */
//一个栈存放入栈元素，一个栈存放出栈元素
public class CQueue {
    Stack<Integer> ph;
    Stack<Integer> pp;
    public CQueue() {
        ph=new Stack<>();
        pp=new Stack<>();
    }
    
    public void appendTail(int value) {
        ph.push(value);
    }
    
    public int deleteHead() {
        //为空说明弹出栈还没有元素就把压入栈的元素加入进去
        if(pp.isEmpty()){
            while ( !ph.isEmpty() ){
                pp.push( ph.pop());
            }
        }
        return pp.isEmpty() ? -1 : pp.pop();
    }
}
