/**
 * Created by L.jp
 * Description:编写一个函数，输入是一个无符号整数（以二进制串的形式），返回其二进制表达式中数字位数为 '1' 的个数（也被称为 汉明重量).）。
 * User: 86189
 * Date: 2023-02-13
 * Time: 23:53
 */
public class Solution {
    public int hammingWeight(int n) {
        /*int res = 0;
        while(n != 0) {
            res += n & 1;
            n >>>= 1;
        }
        return res;*/
        //可以用n&n-1的做法，n&n-1=n-1，这样每&一次，就可以减少一个1，就让计数器+1
        int count=0;
        while(n!=0){
            n=n&n-1;
            count++;
        }
        return count;
    }
}
