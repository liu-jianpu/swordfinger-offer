/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-12-01
 * Time: 23:42
 */
class ListNode {
      int val;
      ListNode next;
      ListNode(int x) {
          val = x;
          next = null;
      }
}
public class Solution {
    ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        ListNode slow=headA;
        ListNode fast = headB;
        //slow和fast同时走，各自走到尽头之后回到对方的起点，继续走，直到两个节点相等，返回即可
        while ( slow!=fast ){
            slow=slow!=null ? slow.next : headB;
            fast=fast!=null ? fast.next :headA;
        }
        return slow;
    }
}
