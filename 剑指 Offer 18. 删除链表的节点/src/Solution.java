/**
 * Created by L.jp
 * Description:给定单向链表的头指针和一个要删除的节点的值，定义一个函数删除该节点。
 *
 * 返回删除后的链表的头节点。
 * User: 86189
 * Date: 2022-12-01
 * Time: 23:01
 */
class ListNode {
      int val;
      ListNode next;
      ListNode(int x) { val = x; }
  }
public class Solution {
    public ListNode deleteNode(ListNode head, int val) {
        if(head==null){
            return head;
        }
        if(head.val == val){
            return head.next;
        }
        ListNode pre=head;
        ListNode cur=head.next;
        while ( cur!=null && cur.val!=val ){
            pre=cur;
            cur=cur.next;
        }
        if(cur!=null){
            pre.next = cur.next;
        }
        return head;
    }
}
