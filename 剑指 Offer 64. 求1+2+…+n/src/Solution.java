/**
 * Created by L.jp
 * Description:求 1+2+...+n ，要求不能使用乘除法、for、while、if、else、switch、case等关键字及条件判断语句（A?B:C）。
 * User: 86189
 * Date: 2023-02-04
 * Time: 23:54
 */
public class Solution {
    //正常就是
   /* public int sumNums(int n) {
        if(n==1){
            return n;
        }
        return n+sumNums(n-1);
    }*/
    //但是不能用if语句，不能用循环，不能使用乘除法，所以就直接抛弃了等差数列求和公式和这个if语句还有循环写法
    //对于if的逻辑，我们可以使用&&运算，如果前者为假那么不执行后者，这就是if的逻辑
    //我们又要知道这个1是特殊情况，必须特殊处理，所以我们只处理大于1的情况，当n==1时可以直接返回n
    public int sumNums(int n) {
        //当n=1时,不会执行递归
        //对于逻辑运算，运算符旁边必须是布尔表达式
        boolean flag=n>1 && (n+=sumNums(n-1))>0;  //n+=sumNums(n-1)就是把n当做最终的结果累加
        return n;
    }
}
