import javax.swing.*;
import java.sql.ResultSet;

/**
 * Description:给定一棵二叉搜索树，请找出其中第 k 大的节点的值。
 * User: 86189
 * Date: 2022-12-14
 * Time: 23:59
 */
class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) { val = x; }
  }
public class Solution {
    //二叉搜索树中序遍历左根右是递增顺序的，而中序反过来就是右根左就是递减顺序的
    //所以按照右根左的顺序查找第k大，遍历一个k就--，直到k=0就找到了
    
    int res,k;
    public int kthLargest(TreeNode root, int k) {
        this.k=k;
        dfs(root);
        return res;
    }
    public void dfs(TreeNode root) {
        if(root == null){
            return ;
        }
        dfs( root.right );
        //k为0就直接返回
        if(k==0){
            return;
        }
        if(--k==0) res=root.val;
        dfs( root.left );
    }

}
