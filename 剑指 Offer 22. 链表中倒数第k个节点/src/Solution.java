/**
 * Created by L.jp
 * Description:输入一个链表，输出该链表中倒数第k个节点。为了符合大多数人的习惯，本题从1开始计数，
 * 即链表的尾节点是倒数第1个节点。
 *
 * 例如，一个链表有 6 个节点，从头节点开始，它们的值依次是 1、2、3、4、5、6。
 * 这个链表的倒数第 3 个节点是值为 4 的节点。
 * User: 86189
 * Date: 2022-12-01
 * Time: 23:14
 */
class ListNode {
      int val;
      ListNode next;
      ListNode(int x) { val = x; }
  }
public class Solution {
    //快慢指针，让fast和slow起始位置位于头结点，然后让fast先走k步，然后fast和slow同时走，直到fast为null时，slow位置就是倒数第k个位置
    public ListNode getKthFromEnd(ListNode head, int k) {
        ListNode slow=head;
        ListNode fast=head;
        while ( fast!=null && k>0 ){
            fast=fast.next;
            k--;
        }
        while ( fast!=null ){
            fast=fast.next;
            slow=slow.next;
        }
        return slow;
    }

}
