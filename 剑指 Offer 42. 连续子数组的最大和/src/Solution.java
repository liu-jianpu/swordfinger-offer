package src;

/**
 * Created by L.jp
 * Description:
 * User: 86189
 * Date: 2022-11-25
 * Time: 23:59
 */
public class Solution {
    public int maxSubArray(int[] nums) {
        //法一：动归解法：
        /*
        if(array.length==1){
            return array[0];
        }
        //表示前i项连续数组的最大和
        int[] dp=new int[array.length];
        //状态初始化
        dp[0]=array[0];
        //最大值初始化
        int max=array[0];
        //遍历数组求出连续子数组的最大和，因为第一项已经给出，所以直接从数组第二项开始
        for(int i=1;i<array.length;i++){
            dp[i]=Math.max(dp[i-1]+array[i],array[i]);
            if(max<dp[i]){
                max=dp[i];
            }
        }
        return max;
        */
        //法二：由于dp[i]只跟dp[i-1]有关，所以可以优化，用一个total变量维护dp[i]的dp[i-1]的值
//        我们可以发现当前状态只跟上一个状态有关，所以我们可以只用一个int来代替dp数组，即total
//        如果total<0，那么这个时候就total=array[i]
//        如果total>0，那么就total=total+array[i]
//        然后实时跟max比较，更新最大值即可
        int total=0;//记录当前累计的和
        int max=nums[0];
        for (int num:nums) {
            total= Math.max(total+num,num);//试着去加上当前值，如果加上后比当前值小了，那么累计和就变成了num
            //或者
//            if(total<0){
//                total=num;//如果之前total累计的和>=0,说明当前数据+total，有利于整体增大
//            }else{
//                total+=num;//如果之前累计的和<0,说明当前数据+total，不利于整体增大,丢弃之前的所有值
//            }
            max=Math.max(total,max);//取得累计和跟max的最大值，不断更新
        }
        return max;
    }
}
