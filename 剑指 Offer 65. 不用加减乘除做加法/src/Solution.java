/**
 * Created by L.jp
 * Description:写一个函数，求两个整数之和，要求在函数体内不得使用 “+”、“-”、“*”、“/” 四则运算符号。
 * User: 86189
 * Date: 2023-02-16
 * Time: 23:46
 */
public class Solution {
    public int add(int a, int b) {
        while (b != 0) {
            int temp = a ^ b;
            b = (a & b) << 1;
            a = temp;
        }
        return a;
    }
}

