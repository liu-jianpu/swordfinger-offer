/**
 * Created by L.jp
 * Description:地上有一个m行n列的方格，从坐标 [0,0] 到坐标 [m-1,n-1]
 * 一个机器人从坐标 [0, 0] 的格子开始移动，它每次可以向左、右、上、下移动一格（不能移动到方格外），
 * 也不能进入行坐标和列坐标的数位之和大于k的格子。例如，当k为18时，机器人能够进入方格 [35, 37] ，
 * 因为3+5+3+7=18。但它不能进入方格 [35, 38]，因为3+5+3+8=19。请问该机器人能够到达多少个格子？

 // 1 <= n,m <= 100
 //0 <= k <= 20
 * User: 86189
 * Date: 2022-12-08
 * Time: 23:58
 */
public class Solution {
    //回溯dfs,只要坐标符合位置，没有被访问过就可以算作一个可以达到的格子
    int res=0;
    public int movingCount(int m, int n, int k) {
        boolean[][] isVisited=new boolean[m][n];
        //从0,0位置开始走
        dfs(0,0,m,n,k,isVisited);
        return res;
    }
    public void dfs(int i,int j,int m,int n,int k,boolean[][] isVisited){
        //判断越界和是否访问过
        if ( i<0 || i>=m || j<0 || j>=n || isVisited[i][j] ){
            return;
        }
        //没有被访问过就可以走，先记录被访问过
        isVisited[i][j]=true;
        int sum=i%10+i/10+j%10+j/10;
        if(sum>k){
            return;
        }
        //以上条件都符合，那么就可以走这一个格子，数量++
        res++;
        //遍历四周
        dfs( i-1,j,m,n,k,isVisited );
        dfs( i,j+1,m,n,k,isVisited );
        dfs( i+1,j,m,n,k,isVisited );
        dfs( i,j-1,m,n,k,isVisited );
    }

   
}
