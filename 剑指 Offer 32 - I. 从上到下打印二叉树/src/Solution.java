import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by L.jp
 * Description:从上到下打印出二叉树的每个节点，同一层的节点按照从左到右的顺序打印。
 *
 *
 * User: 86189
 * Date: 2022-11-18
 * Time: 0:00
 */
class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) { val = x; }
}
public class Solution {
    public int[] levelOrder(TreeNode root) {
        if(root==null){
            return new int[0];
        }
        Queue<TreeNode> nodeQueue=new LinkedList<>();
        nodeQueue.add(root);
        ArrayList<Integer> nodes=new ArrayList<>();
        while ( !nodeQueue.isEmpty() ){
            TreeNode tmp=nodeQueue.poll();
            nodes.add(tmp.val);
            if(tmp.left!=null){
                nodeQueue.add( tmp.left );
            }
            if(tmp.right!=null){
                nodeQueue.add(tmp.right);
            }
        }
        int[] ret=new int[nodes.size()];
        for(int i = 0; i <nodes.size(); i++){
            ret[i] = nodes.get(i);
        }
        return ret;
    }

}
