import java.util.Arrays;

/**
 * Created by L.jp
 * Description:从若干副扑克牌中随机抽 5 张牌，判断是不是一个顺子，
 * 即这5张牌是不是连续的。2～10为数字本身，A为1，J为11，Q为12，K为13，而大、小王为 0 ，
 * 可以看成任意数字。A 不能视为 14。

 * User: 86189
 * Date: 2022-12-19
 * Time: 23:18
 */

//数组长度为 5
//数组的数取值为 [0, 13] .
public class Solution {
    //数组长度是5，如果是顺子，那么除了大小王之外，num[4]-num[0]应该<5才是顺子，如果含有大小王的话，那么也要满足这个条件，、
    // 而且除了大小王之外不能有重复的
    public boolean isStraight(int[] nums) {
        //先排序
        Arrays.sort(nums);
        //记录大小王的数量
        int zeroCount=0;
        //那么nums[zeroCount]就是第一个非0的位置，排完序后就是最小值
        for(int i = 0; i < 4; i++){
            if(nums[i] == 0) zeroCount++;
            else if(nums[i]==nums[i+1]) return false;
        }
        return nums[4]-nums[zeroCount] <5;
    }
}
