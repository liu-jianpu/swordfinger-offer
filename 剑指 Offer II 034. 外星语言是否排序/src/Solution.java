/**
 * Created by L.jp
 * Description:某种外星语也使用英文小写字母，但可能顺序 order 不同。字母表的顺序（order）是一些小写字母的排列。
 *
 * 给定一组用外星语书写的单词 words，以及其字母表的顺序 order，只有当给定的单词在这种外星语中按字典序排列时，返回 true；否则，返回 false。
 *

 * User: 86189
 * Date: 2023-07-16
 * Time: 23:56
 */

/*题目是要判断 words[]中每个单词的“value”是否在数组中是升序(即 words[0]<words[1]<...<words[n]则为true)，
value就是靠那个字母表算出来的，而不是判断每个单词中的字母是否升序，*/
public class Solution {
    public  boolean isAlienSorted(String[] words, String order) {
        //从第二个字符串开始比较，一开始就是第二个字符串与第一个字符串比较
        for(int i = 1; i < words.length; i++){
            if(!isSorted(words[i-1],words[i],order)) return false;
        }
        return true;
    }
    public  boolean isSorted(String word1, String word2, String order){
        for(int i = 0; i < word1.length(); i++) {
            if(i >= word2.length()) return false;
            if(order.indexOf(word1.charAt(i)) > order.indexOf(word2.charAt(i))) return false;
            if(order.indexOf(word1.charAt(i)) < order.indexOf(word2.charAt(i))) return true;
        }
        return true;
    }
}
