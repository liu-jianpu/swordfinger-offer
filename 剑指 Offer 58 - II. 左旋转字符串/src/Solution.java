import java.util.Arrays;

/**
 * Created by L.jp
 * Description:字符串的左旋转操作是把字符串前面的若干个字符转移到字符串的尾部。
 * 请定义一个函数实现字符串左旋转操作的功能。比如，输入字符串"abcdefg"和数字2，
 * 该函数将返回左旋转两位得到的结果"cdefgab"。
 *

 * User: 86189
 * Date: 2022-11-15
 * Time: 8:27
 */
public class Solution {
    //字符串切分函数
    /*public static String reverseLeftWords(String s, int n) {
        return s.substring(n,s.length())+s.substring(0,n);
    }*/
    
    //字符串拼接
    /*public static String reverseLeftWords(String s, int n){
        StringBuilder stringBuilder=new StringBuilder();
        *//*for(int i=n;i<s.length();i++){
            stringBuilder.append(s.charAt( i ));
        }
        for(int i=0;i<n;i++){
            stringBuilder.append(s.charAt( i ));
        }
        return stringBuilder.toString();*//*
        //优化
        for(int i=n;i<n+s.length();i++){
            stringBuilder.append(s.charAt( i%s.length() ));
        }
        return stringBuilder.toString();
    }*/
    
    //三段翻转法
    public static void reverse(char[] chars,int  left,int right){
        while ( left<right ){
            char tmp=chars[left];
            chars[left]=chars[right];
            chars[right]=tmp;
            left++;
            right--;
        }
    }
    public static String reverseLeftWords(String s, int n){
        char[] chars=s.toCharArray();
        //翻转整体
        reverse( chars,0,s.length()-1 );
        //翻转前len-n个
        reverse( chars,0,s.length()-n-1 );
        //翻转后n个
        reverse( chars,s.length()-n,s.length()-1 );
        return new String(chars);
    }
    public static void main (String[] args) {
        String s="abcdefg";
        int n=2;
        System.out.println( reverseLeftWords( s , n ) );
    }
}
