/**
 * Created by L.jp
 * Description:在一个 n * m 的二维数组中，
 * 每一行都按照从左到右非递减的顺序排序，
 * 每一列都按照从上到下非递减的顺序排序。请完成一个高效的函数，
 * 输入这样的一个二维数组和一个整数，判断数组中是否含有该整数。
 * User: 86189
 * Date: 2022-11-11
 * Time: 15:51
 */
public class Solution {
    public static boolean findNumberIn2DArray(int[][] matrix, int target) {
        if(matrix.length==0){
            return false;
        }
        int n=matrix.length;
        int m=matrix[0].length-1;
        int i=0,j=m;
        while(i<n && j>=0){
            if(matrix[i][j]>target){
                j--;
            }else if(matrix[i][j]<target){
                i++;
            }else{
                return true;
            }
        }
        return false;
    }
    
    public static void main(String[] args) {
        int[][] matrix= {{1, 4, 7, 11, 15}, {2, 5, 8, 12, 19}, {3, 6, 9, 16, 22},
                         {10, 13, 14, 17, 24}, {18, 21, 23, 26, 30}};
        System.out.println(findNumberIn2DArray(matrix, 20));
    }
}
