import java.util.Stack;

/**
 * Created by L.jp
 * Description:定义栈的数据结构，
 * 请在该类型中实现一个能够得到栈的最小元素的 min 函数在该栈中，调用 min、push 及 pop 的时间复杂度都是 O(1)。
 * User: 86189
 * Date: 2022-11-13
 * Time: 23:58
 */
public class MinStack {
    Stack<Integer> minStack;
    Stack<Integer> stack;
    public MinStack() {
        minStack=new Stack<>();
        stack=new Stack<>();
    }
    
    public void push(int x) {
        stack.push( x );
        if(minStack.isEmpty() || x<min()){
            minStack.push( x );
        }
    }
    
    public void pop() {
        if(stack.pop()==min()){
            minStack.pop();
        }
    }
    
    public int top() {
        return stack.peek();
    }
    
    public int min() {
        return minStack.peek();
    }
}
