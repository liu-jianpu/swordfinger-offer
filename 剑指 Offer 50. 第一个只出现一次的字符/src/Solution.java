import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
/**
 * Created by L.jp
 * Description:在字符串 s 中找出第一个只出现一次的字符。如果没有，返回一个单空格。
 * s 只包含小写字母。
 * User: 86189
 * Date: 2022-11-16
 * Time: 2:05
 */
public class Solution {
    //哈希表先存储每个字符的次数，然后再次遍历字符串找到第一个出现一次的字符，然后直接返回
    /*public char firstUniqChar(String s) {
        if(s.length()==0){
            return ' ';
        }
        char[] chars=s.toCharArray();
        Map<Character,Integer> map=new HashMap<>();
        int count=0;
        for(int i = 0; i <chars.length; i++){
            if(!map.containsKey(chars[i])){
                map.put( chars[i],1 );
            }else{
                count=map.get( chars[i] );
                count++;
                map.put(chars[i],count);
            }
        }
        for(int i = 0; i < chars.length; i++){
            if(map.get( chars[i] )==1){
               return chars[i];
            }
        }
        return ' ';
    }*/
    
    //字典数组，本身是用来统计字符出现的次数的，先存储每一个字符，然后遍历字典数组找到第一个次数等于1的字符
   /* public char firstUniqChar(String s) {
        if(s.equals( "" )){
            return ' ';
        }
        //字典数组存储每一个字符出现的次数
        int[] dic=new int[26];
        char[] chars=s.toCharArray();
        for(int i = 0; i < chars.length; i++) {
            //利用当前字符减去a字符就是每一个下标
            dic[chars[i]-'a']++;
        }
        //遍历数组，找到次数为1的字符
        for(int i = 0; i < chars.length; i++){
            if(dic[chars[i] - 'a'] ==1){
                return chars[i];
            }
        }
        return ' ';
    }*/
    
    //哈希表存储字符与真值，第一次遍历字符串，如果包含了这个字符，真值就为false，不包含就为true,
    //第二次遍历主串，如果对应的哈希表的value值为true，那么就返回这个字符
    public char firstUniqChar(String s) {
        Map<Character, Boolean> map = new HashMap<>();
        for(int i = 0; i <s.length(); i++){
            map.put( s.charAt( i ),!map.containsKey( s.charAt(i) ) );
        }
        for(int i = 0; i <s.length(); i++){
            if(map.get( s.charAt( i ) )){
                return s.charAt( i );
            }
        }
        return ' ';
    }
}
