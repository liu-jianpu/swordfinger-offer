import java.io.StringReader;

/**
 * Created by L.jp
 * Description:请实现一个函数，用来判断一棵二叉树是不是对称的。如果一棵二叉树和它的镜像一样，那么它是对称的。
 * User: 86189
 * Date: 2022-11-21
 * Time: 23:47
 */
class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) { val = x; }
  }
public class Solution {
    public boolean isSymmetric(TreeNode root) {
        return root == null || dfs( root.left , root.right );
    }
    
    private boolean dfs (TreeNode left , TreeNode right) {
        if(left==null && right==null){
            return true;
        }
        if(left==null || right==null || left.val!=right.val){
            return false;
        }
        return dfs( left.left,right.right ) && dfs( left.right,right.left );
    }
}
