/**
 * Created by L.jp
 * Description:输入一个英文句子，翻转句子中单词的顺序，但单词内字符的顺序不变。为简单起见，
 * 标点符号和普通字母一样处理。例如输入字符串"I am a student. "，则输出"student. a am I"。
 * User: 86189
 * Date: 2022-12-04
 * Time: 0:34
 */
public class Solution {
    //原地翻转
    /*//删除首尾以及中间多余空格
    public StringBuilder remove(String s){
        int start=0;
        int end=s.length()-1;
        StringBuilder tmp=new StringBuilder();
        //去除首尾空格
        while ( s.charAt(start)==' '){start++;}
        while ( s.charAt( end )==' ' ){end--;}
        while ( start<=end ){
            //当前字符不是空格，或者当前空格是字符但是拼接字符串的最后一个位置不是空格说明一个单词拼接结束，再拼接一个该有的空格，多余的空格不会拼接
            if ( s.charAt(start)!=' ' || tmp.charAt( tmp.length()-1 )!=' ') {
                tmp.append( s.charAt( start ) );
            }
            start++;
        }
        //返回最终的结果
        return tmp;
        
    }
    //翻转整个字符串
    public void reverse(StringBuilder tmp,int start,int end){
        while ( start<end ){
            char c=tmp.charAt(start);
            tmp.setCharAt( start,tmp.charAt( end ) );
            tmp.setCharAt( end,c);
            start++;
            end--;
        }
    }
    //翻转单个单词
    //在整个字符串中定住一个单词开始的地方，找到空格，遇到空格说明可以翻转一个单词了
    public void reverseEach(StringBuilder sb){
        int start = 0;
        int end = 1;
        int n = sb.length();
        while (start < n) {
            while (end < n && sb.charAt(end) != ' ') {
                end++;
            }
            reverse(sb, start, end - 1);
            start = end + 1;
            end = start + 1;
        }
    }
    public String reverseWords(String s) {
        //去除首尾多余的空格
        StringBuilder sb=remove( s );
        //翻转整个字符串
        reverse( sb,0,s.length()-1 );
        //翻转单个单词
        reverseEach( sb );
        return sb.toString();
    }*/
    
    public String reverseWords(String s) {
        
        //思路：
        //首先要明白翻转的意思，即单词翻转，但是单词的字母不换位置
        //那么我们就可以从后往前拼接单词，怎么拼接呢，利用StringBuilder,只要单词不是无空格的字符串”“，就拼接
        //那么我们还要去除多余的空格，中间去除多余的空格简单，直接创建字符串数组，以空格分隔各个数组元素
        //拼接完后，我们需要把他转换为字符串，然后利用trim()去除首尾的字符串
        //按空格分割字符串单词，达到去除中间空格的方法
        String[] strs = s.split(" ");
        StringBuilder sb = new StringBuilder();
        //从后往前拼接
        for(int i=strs.length - 1; i >= 0; i--){
            //如果是有单词的，那么就拼接
            if(!strs[i].equals("")){
                sb.append(strs[i]).append(" ");//拿出来的单词不带空格，但是我们要求单词之间存在一个空格，就补上
            }
        }
        return sb.toString().trim();//去除首尾多余的空格
        
    }
}
